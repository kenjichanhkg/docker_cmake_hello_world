#!/usr/bin/env bash

rm -fr ./build && \
mkdir -p "./build" && \
cd "./build" && \
cmake3 .. && \
time make -j`nproc`
