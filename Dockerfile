FROM centos:7 AS the_build
COPY . /usr/test/src
WORKDIR /usr/test/src

# RUN yum install sudo -y
# RUN yum install git -y
RUN yum install make -y
RUN ./development.sh
# RUN ./others.sh
RUN ./build.sh