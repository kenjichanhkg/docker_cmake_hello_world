
mkdir -p ~/dependency_build && pushd ~/dependency_build

yum install -y epel-release
# yum install -y zlib-devel

# cmake
yum install -y cmake3

yum install -y centos-release-scl
yum install -y devtoolset-9-gcc devtoolset-9-gcc-c++
echo "source scl_source enable devtoolset-9" >> ~/.bashrc
source scl_source enable devtoolset-9

popd

