
mkdir -p ~/dependency_build && pushd ~/dependency_build

install_libary_using_cmake(){
    pushd .

    echo going to install $1
    rm -fR $1 && \
    git clone $2 && \
    cd $1 && \
    git checkout $3 && \
    mkdir build && cd build && \
    cmake .. && \
    time make -j$(nproc) && \
    sudo make install

    popd
}

install_libary_using_cmake fmt https://github.com/fmtlib/fmt.git tags/6.0.0
install_libary_using_cmake json https://github.com/nlohmann/json.git tags/v3.6.1
install_libary_using_cmake spdlog https://github.com/gabime/spdlog.git 53ca5b287047d4394417c0b6a3940afe9a942698

popd

